# BIO465_Project - Fire, Rain, and Plant Abundance 


## Project

The Fire, Rain, and Plant Abundance porject focused on answer the question of if fire pressure was required to increase biodiveristy within the Simpson Desert of Australia.
The plant and seed abundance, as well as the rainfall amount from 2004 to 2011 was used to analysis if a wildfire in the same region enhanced the biodiversity within the region. 
The wildfire happened in 2001/2002. 

## Repository Contents

This repository contains 3 files: 

 * 1) An R markdown file that containes the that was used to do the analysis.  

 * 2) A html of the compiled R markdown file.  
 	- Download this file to view it. (This file will not show up in the normal repository console)

 * 3) A pdf file of the compiled R markdown.  


## Data

We used data from the TERN AEKOS data portal. 
In order to run this code you will need to download the data first.  

[Data](http://www.aekos.org.au/index.html#/search-results/list/dataset-details-s?datasetId=au.org.aekos.shared.api.model.dataset.SharedSearchResult:115122&q=%5B%7B%22columnNameAekos%22:%22text%22,%22columnNameShared%22:%22text%22,%22operator%22:%22FreeTextBoost%22,%22value%22:%22Simpson%20Desert%22,%22type%22:%22TEXT%22%7D%5D)

or directly: http://www.aekos.org.au/index.html#/search-results/list/dataset-details-s?datasetId=au.org.aekos.shared.api.model.dataset.SharedSearchResult:115122&q=%5B%7B%22columnNameAekos%22:%22text%22,%22columnNameShared%22:%22text%22,%22operator%22:%22FreeTextBoost%22,%22value%22:%22Simpson%20Desert%22,%22type%22:%22TEXT%22%7D%5D


